package net.stemteam.crabdesigner;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;

public class FXMLController implements Initializable {
    
    @FXML
    private AnchorPane root;
    
    @FXML
    private ToolBar toolBar;
    
    @FXML
    private Button buttonHello;
    
    @FXML
    private AnchorPane designerPane;
    
    private ModelCanvas canvas;
    
    @FXML
    private void handleButtonAddTableAction(ActionEvent event) {
        System.out.println("Add table to model");
        buttonHello.setText("World");
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // цвет фона панели диаграмм
        designerPane.setStyle("-fx-background-color: white;");
        
        // создаем канвас для рисования диаграмм
        canvas = new ModelCanvas(designerPane);
        canvas.getGraphicsContext2D().strokeLine(10, 10 ,100, 100);
        designerPane.getChildren().add(canvas);
        
        
        
        
    }    
}
