/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.crabdesigner;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;

/**
 * Канвас диаграмм
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class ModelCanvas extends Canvas {

    public ModelCanvas(AnchorPane designerPane) {
        super(designerPane.getWidth(), designerPane.getHeight());

        // делаем привязку высоты и ширины
        widthProperty().bind(designerPane.widthProperty());
        heightProperty().bind(designerPane.heightProperty());
        
        
        // на изменение размеров окна - перерисовка канваса
        InvalidationListener listener = new InvalidationListener() {
            @Override
            public void invalidated(Observable o) {
                repaintContext();
            }
        };
        widthProperty().addListener(listener);
        heightProperty().addListener(listener);
    }
    
    /**
     * Перерисовка контента
     */
    public void repaintContext() {
        System.out.println("repaint");
 
        GraphicsContext gc = getGraphicsContext2D();
        gc.clearRect(0, 0, getWidth(), getHeight()); // Очищаем полотно  

        gc.strokeLine(0,0,50,50);
        
//        for (int i = 0; i < f.size(); i++) {
//            MyLabel n1 = f.get(i);
//            MyLabel n2 = s.get(i);
//            gc.strokeLine(n1.getCenterX(), n1.getCenterY(), n2.getCenterX(), n2.getCenterY());
//        }
    }

}
